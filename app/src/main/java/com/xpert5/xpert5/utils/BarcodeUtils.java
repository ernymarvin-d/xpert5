package com.xpert5.xpert5.utils;

import android.util.SparseArray;

import com.google.android.gms.vision.barcode.Barcode;

import java.util.HashMap;

/**
 * Created by Erny on 16/04/2017.
 */

public class BarcodeUtils {

    public static final String ADDRESS = "address";
    public static final String EMAIL = "email";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String COMPANY = "company";
    public static final String OCCUPATION = "occupation";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String FAX_NUMBER = "faxNumber";
    public static final String WEBSITE = "website";

    public static HashMap<String,String> getInformationFromBarcode(SparseArray<Barcode> barcodes) {

        HashMap<String, String> info = new HashMap<>();

        try {
            info.put(ADDRESS, barcodes.valueAt(0).contactInfo.addresses[0].addressLines[0]);
            info.put(EMAIL, barcodes.valueAt(0).contactInfo.emails[0].address);
            info.put(FIRST_NAME, barcodes.valueAt(0).contactInfo.name.first);
            info.put(LAST_NAME, barcodes.valueAt(0).contactInfo.name.last);
            info.put(COMPANY, barcodes.valueAt(0).contactInfo.organization);
            info.put(OCCUPATION, barcodes.valueAt(0).contactInfo.title);

            if (barcodes.valueAt(0).contactInfo.phones.length >= 3) {
                info.put(PHONE_NUMBER, barcodes.valueAt(0).contactInfo.phones[0].number);
                info.put(MOBILE_NUMBER, barcodes.valueAt(0).contactInfo.phones[1].number);
                info.put(FAX_NUMBER, barcodes.valueAt(0).contactInfo.phones[2].number);
            } else {
                info.put(PHONE_NUMBER, barcodes.valueAt(0).contactInfo.phones[0].number);
                info.put(MOBILE_NUMBER, "");
                info.put(FAX_NUMBER, "");
            }

            if (barcodes.valueAt(0).contactInfo.urls.length > 0) {
                info.put(WEBSITE, barcodes.valueAt(0).contactInfo.urls[0]);
            } else {
                info.put(WEBSITE, "");
            }

            return info;
        } catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

}
