package com.xpert5.xpert5.utils;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.AddSheetRequest;
import com.google.api.services.sheets.v4.model.AppendValuesResponse;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetRequest;
import com.google.api.services.sheets.v4.model.BatchUpdateSpreadsheetResponse;
import com.google.api.services.sheets.v4.model.Request;
import com.google.api.services.sheets.v4.model.Sheet;
import com.google.api.services.sheets.v4.model.SheetProperties;
import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.xpert5.xpert5.Splash;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.support.v4.app.ActivityCompat.startActivityForResult;

/**
 * Created by Erny on 01/05/2017.
 */

public class MySheet {

    public static final String TAG = ".xpert5.utils.MySheet";
    public static final String SPREADSHEET_ID = "1SRn1farNnQRsdrKIyCcJSVGGKemMvZNw0tiePuwZPB0";

    public static void createSheet(Sheets service, String attendanceSheet) throws IOException{
        SheetProperties sheetProperties = new SheetProperties();
        sheetProperties.setTitle(attendanceSheet);

        AddSheetRequest addSheetRequest = new AddSheetRequest();
        addSheetRequest.setProperties(sheetProperties);

        Request request = new Request();
        request.setAddSheet(addSheetRequest);

        List<Request> data = new ArrayList<>();
        data.add(request);

        BatchUpdateSpreadsheetRequest requestBody = new BatchUpdateSpreadsheetRequest();
        requestBody.setRequests(data);


        Sheets.Spreadsheets.BatchUpdate request1 = service.spreadsheets().batchUpdate(MySheet.SPREADSHEET_ID, requestBody);
        BatchUpdateSpreadsheetResponse response = request1.execute();

        addColumnHeader(service, attendanceSheet, response.getSpreadsheetId());
    }

    private static void addColumnHeader(Sheets service, String attendanceSheet, String sheetId){
        String range = attendanceSheet;

        List<List<Object>> values = new ArrayList<>();
        List<Object> items = new ArrayList<>();

        items.add("LASTNAME");
        items.add("FIRSTNAME");
        items.add("ADDRESS");
        items.add("EMAIL");
        items.add("OCCUPATION");
        items.add("PHONE NUMBER");
        items.add("MOBILE NUMBER");
        items.add("WEBSITE");
        items.add("TIME-IN");
        items.add("TIME-OUT");

        values.add(items);

        ValueRange content = new ValueRange();
        content.setValues(values);
        content.setRange(range);

        try {
            Sheets.Spreadsheets.Values.Append request = service.spreadsheets().values().append(sheetId, range, content);
            request.setValueInputOption("RAW");

            AppendValuesResponse response = request.execute();
            Log.i(TAG, response.toString());
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static boolean hasSheet(Sheets service, String attendanceSheet) throws IOException {
        Sheets.Spreadsheets.Get request = service.spreadsheets().get(MySheet.SPREADSHEET_ID);
        request.setRanges(Arrays.asList(attendanceSheet));

        try {
            Spreadsheet response = request.execute();
            if (response.size() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return false;
        } catch (GoogleJsonResponseException ex) {
            Log.i("Error Do in post", ex.getMessage().toString());
            return false;
        }
    }

}
