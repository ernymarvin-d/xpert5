package com.xpert5.xpert5.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Patterns;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.xpert5.xpert5.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by Erny on 16/04/2017.
 */

public class Utils {

    public static final int SCAN_STATUS_LOG_IN = 200;
    public static final int SCAN_STATUS_LOG_OUT = 201;
    public static final int SCAN_STATUS_INVALID = 202;

    private static final String[] SCOPES = { SheetsScopes.SPREADSHEETS };

    public static String getCurrentDate(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss a");
        return simpleDateFormat.format(new Date());
    }

    public static AlertDialog getLoginDialog(Context context, AlertDialog dialog, String name, int status){
        String message = "";
        switch(status){
            case SCAN_STATUS_LOG_IN:
                message = "Welcome " + name;
                break;
            case SCAN_STATUS_LOG_OUT:
                message = "Goodbye " + name;
                break;
            case SCAN_STATUS_INVALID:
                message = "Invalid QR Code SCANNED";
                break;
        }
        dialog = new AlertDialog.Builder(context)
                .setMessage(message)
                .setTitle("Xpert5")
                .create();
        dialog.show();

        return dialog;
    }

    public static String getUserAccount(Context context) {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(context).getAccounts();
        for (Account account : accounts) {
            if (emailPattern.matcher(account.name).matches()) {
                return account.name;
            }
        }
        return "";
    }

    public static String getCurrentAttendanceSheet(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM-d-yyyy");
        return simpleDateFormat.format(new Date());
    }

    public static GoogleAccountCredential getCredentials(Context context) {
        String accountName = getUserAccount(context);

        return GoogleAccountCredential.usingOAuth2(context, Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff())
                .setSelectedAccountName(accountName);
    }

    public static Sheets generateService(GoogleAccountCredential credential) {
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();

        try {
            return new Sheets.Builder(transport, jsonFactory, credential)
                    .setApplicationName("Xpert5DTR").build();
        }catch (Exception e){
            e.getMessage();
        }

        return null;
    }

}
