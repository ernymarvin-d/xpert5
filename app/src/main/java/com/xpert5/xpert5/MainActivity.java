package com.xpert5.xpert5;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.AppendValuesResponse;
import com.google.api.services.sheets.v4.model.UpdateValuesResponse;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.xpert5.xpert5.dialogs.ScanStatusDialog;
import com.xpert5.xpert5.utils.BarcodeUtils;
import com.xpert5.xpert5.utils.MySheet;
import com.xpert5.xpert5.utils.Utils;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// https://docs.google.com/spreadsheets/d/1SRn1farNnQRsdrKIyCcJSVGGKemMvZNw0tiePuwZPB0

public class MainActivity extends AppCompatActivity implements SurfaceHolder.Callback,
        BarcodeDetector.Processor<Barcode>{

    private static final String TAG = "xpert5.MainActivity";

    private boolean mIsProcessingInfo = false;

    private GoogleAccountCredential mCredential;
    private Sheets mService;
    private SurfaceView mCameraView;
    private CameraSource mCameraSource;

    private AlertDialog mStatusAlertDialog;
    private String mAttendanceSheet = "";

    private ScanStatusDialog statusDialog = new ScanStatusDialog();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCredential = Utils.getCredentials(getApplicationContext());
        mAttendanceSheet = Utils.getCurrentAttendanceSheet();
        mService = Utils.generateService(mCredential);

        mCameraView = (SurfaceView) findViewById(R.id.surface_view_camera);
        BarcodeDetector detector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        detector.setProcessor(this);

        mCameraSource = new CameraSource.Builder(this, detector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setAutoFocusEnabled(true)
                .build();
        mCameraView.getHolder().addCallback(this);

    }

    /**
     * An asynchronous task that handles the Google Sheets API call.
     * Placing the API calls in their own task ensures the UI stays responsive.
     */
    private class LoginEmployee extends AsyncTask<Void, Void, String> {
        private HashMap<String, String> employeeInfo = new HashMap<>();
        private ProgressDialog progressDialog;
        LoginEmployee(GoogleAccountCredential credential, HashMap<String, String> employeeInfo) {
            this.employeeInfo = employeeInfo;
        }
        /**
         * Background task to call Google Sheets API.
         * @param params no parameters needed for this task.
         */
        @Override
        protected String doInBackground(Void... params) {
            String employeeName = "";
            try {
                employeeName = addDataToApi();
            } catch (Exception e) {
                e.printStackTrace();
                cancel(true);
            }
            return employeeName;
        }

        private String addDataToApi() throws IOException, GeneralSecurityException {
            String range = mAttendanceSheet;

            List<List<Object>> values = new ArrayList<>();
            List<Object> items = new ArrayList<>();

            items.add( employeeInfo.get(BarcodeUtils.LAST_NAME) );
            items.add( employeeInfo.get(BarcodeUtils.FIRST_NAME) );
            items.add( employeeInfo.get(BarcodeUtils.ADDRESS) );
            items.add( employeeInfo.get(BarcodeUtils.EMAIL) );
            items.add( employeeInfo.get(BarcodeUtils.OCCUPATION) );
            items.add( employeeInfo.get(BarcodeUtils.PHONE_NUMBER) );
            items.add( employeeInfo.get(BarcodeUtils.MOBILE_NUMBER) );
            items.add( employeeInfo.get(BarcodeUtils.WEBSITE) );
            items.add( Utils.getCurrentDate() );
            items.add( "-" );

            values.add(items);

            ValueRange content = new ValueRange();
            content.setValues(values);
            content.setRange(range);

            Sheets.Spreadsheets.Values.Append request = mService.spreadsheets().values().append(MySheet.SPREADSHEET_ID, range, content);
            request.setValueInputOption("RAW");

            try {
                AppendValuesResponse response = request.execute();
                Log.i(TAG, response.toString());
            }catch (UserRecoverableAuthIOException e) {
                startActivityForResult(e.getIntent(), 0);
            }

            return employeeInfo.get(BarcodeUtils.FIRST_NAME);
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(MainActivity.this, "Xpert5 Login", "Verifying account...");
        }

        @Override
        protected void onPostExecute(String employeeName) {
            progressDialog.dismiss();
            statusDialog.popUpStatus(MainActivity.this, employeeName, ScanStatusDialog.SCAN_STATUS_LOG_IN);
            mIsProcessingInfo = false;
        }

        @Override
        protected void onCancelled() {}

    }

    private class LogUserTask extends AsyncTask< Void, Void, List<List<Object>> >{

        private final int STATUS_LOGGED_IN = 100;
        private final int STATUS_LOGGED_OUT = 101;
        private HashMap<String, String> mBarcodeInfo;
        private String mRange;

        LogUserTask(String attendanceSheetTitle, HashMap<String, String> barcodeInfo) {
            this.mRange = attendanceSheetTitle + "!A2:J"; // get from row 2 onwards
            this.mBarcodeInfo = barcodeInfo;
        }

        @Override
        protected List<List<Object>> doInBackground(Void... voids) {
            try {
                Sheets.Spreadsheets.Values.Get request = mService.spreadsheets().values()
                        .get(MySheet.SPREADSHEET_ID, mRange);
                request.setValueRenderOption("UNFORMATTED_VALUE");
                request.setRange(mRange);

                ValueRange response = request.execute();
                return response.getValues();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<List<Object>> response) {
            int onRow = 0;
            int index = 1;

            int employeeStatus = STATUS_LOGGED_OUT;
            String barcodeEmail = mBarcodeInfo.get(BarcodeUtils.EMAIL);

            try {
                for (List<Object> attendanceInfo : response) {
                    index++;

                    String sheetEmail = attendanceInfo.get(3).toString();
                    String timeIn = attendanceInfo.get(8).toString();
                    String timeOut = attendanceInfo.get(9).toString();

                    if (barcodeEmail.equalsIgnoreCase(sheetEmail)
                            && timeOut.equalsIgnoreCase("-")) {
                        employeeStatus = STATUS_LOGGED_IN;
                        onRow = index;
                    }
                }

                switch (employeeStatus) {
                    case STATUS_LOGGED_IN:
                        new LogoutEmployee(onRow).execute();
                        break;
                    case STATUS_LOGGED_OUT:
                        new LoginEmployee(mCredential, mBarcodeInfo).execute();
                }
            } catch (Exception e){
                new LoginEmployee(mCredential, mBarcodeInfo).execute();
            }
        }

    }

    /**
     * An asynchronous task that handles the Google Sheets API call.
     * Placing the API calls in their own task ensures the UI stays responsive.
     */
    private class LogoutEmployee extends AsyncTask<Void, Void, String> {

        private String mRange;
        private HashMap<String, String> employeeInfo = new HashMap<>();
        private ProgressDialog progressDialog;

        LogoutEmployee(int onRow) {
            this.mRange = mAttendanceSheet + "!J" + onRow;
        }

        @Override
        protected String doInBackground(Void... params) {
            String employeeName = "";
            try {
                employeeName = updateSheet();
            } catch (Exception e) {
                e.printStackTrace();
                cancel(true);
            }
            return employeeName;
        }

        private String updateSheet() throws IOException, GeneralSecurityException {
            List<List<Object>> values = new ArrayList<>();
            List<Object> items = new ArrayList<>();

            items.add( Utils.getCurrentDate() );

            values.add(items);

            ValueRange content = new ValueRange();
            content.setValues(values);
            content.setRange(mRange);

            Sheets.Spreadsheets.Values.Update request = mService.spreadsheets().values()
                    .update(MySheet.SPREADSHEET_ID, mRange, content);
            request.setValueInputOption("RAW");
            try {
                UpdateValuesResponse response = request.execute();
                Log.i(TAG, response.toString());
            }catch (UserRecoverableAuthIOException e) {
                startActivityForResult(e.getIntent(), 0);
            }

            return employeeInfo.get(BarcodeUtils.FIRST_NAME);
        }

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(MainActivity.this, "Xpert5", "Verifying account...");
        }

        @Override
        protected void onPostExecute(String employeeName) {
            final Handler handler = new Handler();

            progressDialog.dismiss();
            statusDialog.popUpStatus(MainActivity.this, "", ScanStatusDialog.SCAN_STATUS_LOG_OUT);
            mIsProcessingInfo = false;
        }

        @Override
        protected void onCancelled() {}

    }

    // SurfaceHolder.Callback
    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            mCameraSource.start(mCameraView.getHolder());
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (SecurityException securityException){
            securityException.printStackTrace();
            Intent intent = new Intent(MainActivity.this, Splash.class);
            startActivity(intent);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {}

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) { mCameraSource.stop(); }
    // end SurfaceHolder.Callback

    // BarcodeDetector.Processor
    @Override
    public void release() { }

    @Override
    public void receiveDetections(Detector.Detections<Barcode> detections) {
        final SparseArray<Barcode> barcodes = detections.getDetectedItems();

        if(barcodes.size() != 0 && !mIsProcessingInfo){
            mIsProcessingInfo = true;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    HashMap<String, String> barcodeInfo =
                            BarcodeUtils.getInformationFromBarcode(barcodes);
                    if (barcodeInfo == null) {
                        statusDialog.popUpStatus(MainActivity.this, null, ScanStatusDialog.SCAN_STATUS_INVALID);
                        mIsProcessingInfo = false;
                        return;
                    }
                    new LogUserTask(mAttendanceSheet, barcodeInfo).execute();
                }
            });

        }
    }
    // end BarcodeDetector.Processor

}
