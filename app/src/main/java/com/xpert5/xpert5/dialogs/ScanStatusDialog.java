package com.xpert5.xpert5.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xpert5.xpert5.R;

import static android.R.id.message;
import static android.R.id.title;

/**
 * Created by Erny on 10/05/2017.
 */

public class ScanStatusDialog {

    public static final int SCAN_STATUS_LOG_IN = 200;
    public static final int SCAN_STATUS_LOG_OUT = 201;
    public static final int SCAN_STATUS_INVALID = 202;

    public void popUpStatus(Context context, String name, int status){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.alert_dialog_dtr_message);

        RelativeLayout titleBackground = (RelativeLayout) dialog.findViewById(R.id.bg_title);
        TextView tvMessage = (TextView) dialog.findViewById(R.id.tv_message);

        titleBackground.setBackgroundColor(getTitleBgColor(context, status));
        tvMessage.setText(getMessage(name, status));

        dialog.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 2000);
    }

    private static String getMessage(String name, int status){
        String message = "";
        switch(status){
            case SCAN_STATUS_LOG_IN:
                message = "Welcome " + name;
                break;
            case SCAN_STATUS_LOG_OUT:
                message = "Goodbye " + name;
                break;
            case SCAN_STATUS_INVALID:
                message = "Invalid QR Code SCANNED";
                break;
        }

        return message;
    }

    public static int getTitleBgColor(Context context, int status){
        int color = 0;
        switch(status){
            case SCAN_STATUS_LOG_IN:
                color = ContextCompat.getColor(context, R.color.bg_login);
                break;
            case SCAN_STATUS_LOG_OUT:
                color = ContextCompat.getColor(context, R.color.bg_logout);
                break;
            case SCAN_STATUS_INVALID:
                color = ContextCompat.getColor(context, R.color.bg_invalid);
                break;
        }

        return color;
    }

}
