package com.xpert5.xpert5;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.services.sheets.v4.Sheets;
import com.xpert5.xpert5.utils.MySheet;
import com.xpert5.xpert5.utils.Utils;

import java.io.IOException;

public class Splash extends AppCompatActivity {

    private static final String TAG = "xpert5.Splash";

    private static final int PERMISSION_REQUEST_CAMERA_ACCOUNTS = 100;
    private static final int PERMISSION_REQUEST_AUTHORIZATION = 102;

    CheckSheetTask checkSheetTask = new CheckSheetTask();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ImageView ivLoading = (ImageView) findViewById(R.id.iv_loading);
        setRotateAnimation(ivLoading);

        if (hasRequiredPermission()) {
            checkSheetTask.execute();
        } else {
            requestPermission();
        }
    }

    private void setRotateAnimation(ImageView ivIcon) {
        Animation anim = AnimationUtils.loadAnimation(Splash.this, R.anim.rotate);
        ivIcon.startAnimation(anim);
    }

    private class CheckSheetTask extends AsyncTask<Void, Void, Void> {
        String mAttendanceSheet;
        Sheets mService;
        GoogleAccountCredential mCredential;

        @Override
        protected void onPreExecute() {
            mCredential = Utils.getCredentials(getApplicationContext());
            mAttendanceSheet = Utils.getCurrentAttendanceSheet();
            mService = Utils.generateService(mCredential);

        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{
                if(!MySheet.hasSheet(mService, mAttendanceSheet)){
                    MySheet.createSheet(mService, mAttendanceSheet);
                }
            } catch (UserRecoverableAuthIOException e) {
                startActivityForResult(e.getIntent(), PERMISSION_REQUEST_AUTHORIZATION);
            } catch (IOException ex){
                ex.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(Splash.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                }
            }, 1500);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PERMISSION_REQUEST_CAMERA_ACCOUNTS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    checkSheetTask.execute();
                }else{
                    finish();
                    // TODO add dialog box for reminder that we need camera for this app
                }
                break;
            case PERMISSION_REQUEST_AUTHORIZATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    finish();
                    startActivity(getIntent());
                }else{
                    finish();
                }
                break;
        }
    }

    private void requestPermission(){
        String[] appRequiredPermissions = new String[] {
                Manifest.permission.CAMERA,
                Manifest.permission.GET_ACCOUNTS };

        ActivityCompat.requestPermissions(Splash.this, appRequiredPermissions,
                PERMISSION_REQUEST_CAMERA_ACCOUNTS);
    }

    private boolean hasRequiredPermission(){
        int myCameraPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int myGetAccountPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS);

        if ( myCameraPermission != PackageManager.PERMISSION_GRANTED
                || myGetAccountPermission != PackageManager.PERMISSION_GRANTED) {
            return false;
        }

        return true;
    }
}
